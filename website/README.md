# website
I really dislike developing web applications directly on my machine.
I always and up leaving the `node_modules` or have global package installed and it just clutters up my machine.

Recently, ive started to have the web application files on my local machine, but mount them into a container.
It means that if i need a new package, i need to re-run the build process, but i find its a small price to pay for having a reliable and repeatable process without cluttering up my machine.

This website is just a noddy website with a basic form submission.

NOTE - you'll need to change your `site` key.
Change `Vue.use(VueReCaptcha, { siteKey: 'xxxxxxxxxxxxxxxxxxxxxxxxx' })`  within `src/components/Form.vue` as appropriate.

## Initial Build
On my local machine, i need some stuff to bootstrap the development.

Install node & the npm package manage
```
sudo mkdir -p /usr/local/lib/nodejs
wget https://nodejs.org/dist/v14.17.5/node-v14.17.5-linux-x64.tar.xz -O /tmp/node.tar.xz
sudo tar -xJvf /tmp/node.tar.xz -C /usr/local/lib/nodejs
echo "export PATH=$PATH:/usr/local/lib/nodejs/node-v14.17.5-linux-x64/bin" >> ~/.zshrc
source ~/.zshrc
```

Install the Vue CLI

```
sudo env PATH=$PATH:/usr/local/lib/nodejs/node-v14.17.5-linux-x64/bin npm install -g @vue/cli
```

Bootstrap the project
```
vue create hello-world -d
```

This will take a little while, but will bootstrap a nice basic web app.
The next thing to do is to delete the `node_modules` as I dont want them on my local machine. Already they are `177MB` just with the bootstrap!
```
rm -rf ./hello-world/node_modules
```

You'll need `axios` and `vue-recaptcha-v3` which can be installed with:
```
npm install axios vue-axios vue-recaptcha-v3@^1.9.0
```

## Build

Two `Dockerfiles` are created:
- `dev.Dockerfile` which can be used for developing the web app
- `prod.Dockerfile` which can be used for outputting a production build (minified and all that jazz)

Both of which are used in the `docker-compose` deployment which can be ran with:
```
docker-compose up --build -d
docker-compose exec dev sh
docker-compose exec prod sh
```

Updates to the `src` directory are persisted.
If you need additional `npm` modules, install them into the container and manually copy out the `packages.json`.

So now we have a lightweight*ish* development environment that we can dip in and out of, without worrying about packages.

Unfortunately the `DOCKER_BUILDKIT` isnt properly integrated with `docker-compose` yet, so i'll have to settle for copying the `dist` directory out of the `prod` build for now.

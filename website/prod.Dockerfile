FROM node:lts-alpine as builder

RUN apk add --no-cache \
    python \
    make \
    g++ \
    nano \
    curl

WORKDIR /app

COPY hello-world/package*.json ./
COPY hello-world/babel.config.js ./
COPY hello-world/src ./src

RUN npm install . \
    && npm run build

CMD ["tail", "-F", "anything"]
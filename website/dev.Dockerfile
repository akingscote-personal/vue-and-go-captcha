FROM node:lts-alpine

RUN apk add --no-cache \
        python \
        make \
        g++ \
        nano \
        curl
    # && npm install -g @vue/cli-service

WORKDIR /app


COPY hello-world/package*.json ./
# COPY vue.config.js ./
COPY hello-world/babel.config.js ./
RUN npm install .

CMD ["npm", "run", "serve"]

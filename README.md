# vue-and-go-captcha

This repository demonstrates setting up Captcha V3 in a VueJS application including the server side verification mechanism.

This is useful for form submissions to combat spam submissions.
## server
I'd like to have my own server side validation mechanism for the captcha validation.

NOTE - you'll need to update the code to include your own `secret` key. 
(i should probably add it as a flag to the app...)
Change the `secret` variable in the `validateCaptcha` function inside `internal/webserver.go`.

## Build
You dont actually need `go` installed to build the binary, simply run the following:
```
DOCKER_BUILDKIT=1 docker build -o bin .
```
An the binary will be built for you and placed in `./dist/` directory.

## Bootstrap
Im a big fan of the golang [cobra](https://github.com/spf13/cobra) CLI package.
Also trying to conform to the [standards](https://github.com/golang-standards/project-layout) for project layout.

```
go get -u github.com/spf13/cobra
```

Create a directories:
```
mkdir cmd internal
```

Create a new go module
```
go mod init gitlab.comakingscote-personal/vue-and-go-captcha 
```

Then from there, the app was manually developed.



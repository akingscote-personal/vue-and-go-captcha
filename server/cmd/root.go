
package cmd

import (
	"fmt"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/akingscote-personal/vue-and-go-captcha/internal"
)

var cfgFile string
var logLevel string
var port int
var websiteDir string

// rootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "webserver",
	Short: "webserver will captcha v3 validation",
	Long:  `Run the websever which hosts a VueJS application and server side
	captcha validation`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) { 
		webserver.Serve(port, websiteDir)

		// todo ensure websiteDir actually exists

	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(RootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	RootCmd.PersistentFlags().StringVar(&websiteDir, "websiteDir", "./static", "Full path to website files")
	RootCmd.PersistentFlags().StringVar(&logLevel, "logLevel", "info", "Log Level (debug, info, warn, error)")
	RootCmd.PersistentFlags().IntVar(&port, "port", 8080, "port to listen on")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".emailServer" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".emailServer")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}

	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
		log.Debugln("Using Debug Logging")
	case "info":
		log.SetLevel(log.InfoLevel)
		log.Infoln("Using Info Logging")
	case "warning":
		log.SetLevel(log.WarnLevel)
		log.Warningln("Using Warning Logging")
	case "error":
		log.SetLevel(log.ErrorLevel)
		log.Errorln("Using Error Logging")
	default:
		log.SetLevel(log.InfoLevel)
		log.Infoln("Using Info Logging")
	}

}
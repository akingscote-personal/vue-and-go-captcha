package webserver

import (
	"fmt"
	"net/http"
	"strconv"
	log "github.com/sirupsen/logrus"
	"os"
	"io/ioutil"
	"net/url"
	"time"
	"encoding/json"
)

type CaptchaResponse struct {
	Success     bool      `json:"success"`
	ChallengeTs time.Time `json:"challenge_ts"`
	Hostname    string    `json:"hostname"`
	Score       float64   `json:"score"`
	Action      string    `json:"action"`
}

func Serve(port int, websiteDir string) {
	listenPort := strconv.Itoa(port)
	listenAddress := fmt.Sprintf(":%s", listenPort)

	http.Handle("/", http.FileServer(http.Dir(websiteDir)))
	http.HandleFunc("/myendpoint", myEndpoint)

	log.Infof("Listening on %s...", listenAddress)
	err := http.ListenAndServe(listenAddress, nil)
	if err != nil {
		log.Fatalln(err)
	}

}

func myEndpoint(w http.ResponseWriter, r *http.Request) {
	log.Debugln("Endpoint request...")
	// check for preflight
	if r.Method == "OPTIONS" {
		log.Infoln("OPTIONS request received, likely preflight request...")
		setupHeaders(w, r)
		return
	}
	
    setupHeaders(w, r)

	// here you could also add your honeypot check
	name := r.PostFormValue("name")
	email := r.PostFormValue("email")
	message := r.PostFormValue("message")

	captcha := r.Header.Get("X-Recaptcha-Token")
	if captcha == "" {
		log.Errorln("Captcha Token not received")
		returnError(w, r, "Captcha Token not received")
		return
	} else {
		log.Infoln("Found captcha in incoming request", captcha)
	}
	captchaValid := validateCaptcha(captcha)

	if captchaValid == false {
		log.Errorln("Server side captcha validation invalid")
		returnError(w, r, "Server captcha validation invalid")
		return
	} else {
		log.Infoln("Server side captcha validation successful")
	}

	if isEmpty(name, email,  message) {
		returnError(w, r, "Missing expected fields in request")
		log.Errorln("Missing expected fields in request")
		return
	}
	log.Infoln("Request content looks about right...")

	// do whatever you want here...
}



func isEmpty(ss ...string) (bool) {
	for _, s := range ss {
		if s == "" {
			log.Errorf("Missing expected attribute `%s`", s)
			return true
		}
	}
	return false
}


func setupHeaders(rw http.ResponseWriter, req *http.Request) {
	rw.Header().Set("Content-Type", "application/json")
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	rw.Header().Set("Access-Control-Allow-Headers", "*")
}

func validateCaptcha(captcha_response string) bool {
	secret := "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
	
	//one-line post request/response...
	response, err := http.PostForm("https://www.google.com/recaptcha/api/siteverify", url.Values{
		"secret": {secret},
		"response": {captcha_response}})

	if err != nil {
		log.Errorln("error verifying captcha", err)
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Errorln(err)
	}

	var capResponse CaptchaResponse
	err = json.Unmarshal(body, &capResponse)
	if err != nil {
		log.Errorln("error:", err)
	}
	log.Debugln(capResponse)

	captchaScore := 0.75

	if capResponse.Score < captchaScore {
		log.Errorln("Captcha score less than", captchaScore, capResponse.Score)
		return false
	}

	if capResponse.Success == true {
		log.Infoln("Captcha validation successful - ", captchaScore)
		return true
	} else {
		return false
	}
}

func getenv(key, fallback string) string {
    value := os.Getenv(key)
    if len(value) == 0 {
        return fallback
    }
    return value
}

func returnError(w http.ResponseWriter, r *http.Request, message string) {
    w.WriteHeader(412)
	fmt.Fprint(w, message)
}